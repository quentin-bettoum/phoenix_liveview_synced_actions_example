defmodule SyncedActionsWeb.LiveHelpers do
  alias SyncedActionsWeb.Endpoint
  alias SyncedActions.Fruits.Fruit

  import Phoenix.LiveView, only: [stream_insert: 4, stream_delete: 3]

  @stream_operations [:insert, :delete]

  def broadcast(topic, operation, entity) when operation in @stream_operations do
    Endpoint.broadcast!(topic, "list_update", {operation, entity})
  end

  def handle_list_update(socket, operation, entity) when operation in @stream_operations do
    stream_name = 
      case entity do
        %Fruit{} = _ -> :fruits
        # %Vegetable{} = _ -> :vegetables
      end

    update_stream(socket, operation, stream_name, entity)
  end
  
  defp update_stream(socket, :insert, stream_name, entity), do: stream_insert(socket, stream_name, entity, at: -1)
  defp update_stream(socket, :delete, stream_name, entity), do: stream_delete(socket, stream_name, entity)

  def disable_entity_actions(entity, reason) do
    action = {:locked, reason}
    %{entity | actions: %{edit: action, delete: action}}
  end

  def enable_entity_actions(entity) do
    %{entity | actions: %{edit: true, delete: true}}
  end
end
