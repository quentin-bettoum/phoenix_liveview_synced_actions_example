defmodule SyncedActionsWeb.FruitLive.Show do
  use SyncedActionsWeb, :live_view

  alias SyncedActions.Fruits

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:fruit, Fruits.get_fruit!(id))}
  end

  defp page_title(:show), do: "Show Fruit"
  defp page_title(:edit), do: "Edit Fruit"
end
