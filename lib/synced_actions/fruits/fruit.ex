defmodule SyncedActions.Fruits.Fruit do
  use Ecto.Schema
  import Ecto.Changeset

  schema "fruits" do
    field :color, :string
    field :name, :string
    field :quantity, :integer
    field :actions, :map, virtual: true, default: %{edit: true, delete: true}

    timestamps()
  end

  @doc false
  def changeset(fruit, attrs) do
    fruit
    |> cast(attrs, [:name, :color, :quantity])
    |> validate_required([:name, :color, :quantity])
  end
end
