defmodule SyncedActions.FruitsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `SyncedActions.Fruits` context.
  """

  @doc """
  Generate a fruit.
  """
  def fruit_fixture(attrs \\ %{}) do
    {:ok, fruit} =
      attrs
      |> Enum.into(%{
        color: "some color",
        name: "some name",
        quantity: 42
      })
      |> SyncedActions.Fruits.create_fruit()

    fruit
  end
end
