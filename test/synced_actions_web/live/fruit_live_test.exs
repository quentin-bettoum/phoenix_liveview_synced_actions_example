defmodule SyncedActionsWeb.FruitLiveTest do
  use SyncedActionsWeb.ConnCase

  import Phoenix.LiveViewTest
  import SyncedActions.FruitsFixtures

  @create_attrs %{color: "some color", name: "some name", quantity: 42}
  @update_attrs %{color: "some updated color", name: "some updated name", quantity: 43}
  @invalid_attrs %{color: nil, name: nil, quantity: nil}

  defp create_fruit(_) do
    fruit = fruit_fixture()
    %{fruit: fruit}
  end

  describe "Index" do
    setup [:create_fruit]

    test "lists all fruits", %{conn: conn, fruit: fruit} do
      {:ok, _index_live, html} = live(conn, ~p"/fruits")

      assert html =~ "Listing Fruits"
      assert html =~ fruit.color
    end

    test "saves new fruit", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/fruits")

      assert index_live |> element("a", "New Fruit") |> render_click() =~
               "New Fruit"

      assert_patch(index_live, ~p"/fruits/new")

      assert index_live
             |> form("#fruit-form", fruit: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#fruit-form", fruit: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/fruits")

      html = render(index_live)
      assert html =~ "Fruit created successfully"
      assert html =~ "some color"
    end

    test "updates fruit in listing", %{conn: conn, fruit: fruit} do
      {:ok, index_live, _html} = live(conn, ~p"/fruits")

      assert index_live |> element("#fruits-#{fruit.id} a", "Edit") |> render_click() =~
               "Edit Fruit"

      assert_patch(index_live, ~p"/fruits/#{fruit}/edit")

      assert index_live
             |> form("#fruit-form", fruit: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#fruit-form", fruit: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/fruits")

      html = render(index_live)
      assert html =~ "Fruit updated successfully"
      assert html =~ "some updated color"
    end

    test "deletes fruit in listing", %{conn: conn, fruit: fruit} do
      {:ok, index_live, _html} = live(conn, ~p"/fruits")

      assert index_live |> element("#fruits-#{fruit.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#fruits-#{fruit.id}")
    end
  end

  describe "Show" do
    setup [:create_fruit]

    test "displays fruit", %{conn: conn, fruit: fruit} do
      {:ok, _show_live, html} = live(conn, ~p"/fruits/#{fruit}")

      assert html =~ "Show Fruit"
      assert html =~ fruit.color
    end

    test "updates fruit within modal", %{conn: conn, fruit: fruit} do
      {:ok, show_live, _html} = live(conn, ~p"/fruits/#{fruit}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Fruit"

      assert_patch(show_live, ~p"/fruits/#{fruit}/show/edit")

      assert show_live
             |> form("#fruit-form", fruit: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#fruit-form", fruit: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/fruits/#{fruit}")

      html = render(show_live)
      assert html =~ "Fruit updated successfully"
      assert html =~ "some updated color"
    end
  end
end
