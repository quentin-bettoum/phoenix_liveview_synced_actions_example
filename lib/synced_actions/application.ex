defmodule SyncedActions.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      SyncedActionsWeb.Telemetry,
      # Start the Ecto repository
      SyncedActions.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: SyncedActions.PubSub},
      # Start Finch
      {Finch, name: SyncedActions.Finch},
      SyncedActionsWeb.Presence,
      # Start the Endpoint (http/https)
      SyncedActionsWeb.Endpoint
      # Start a worker by calling: SyncedActions.Worker.start_link(arg)
      # {SyncedActions.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SyncedActions.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    SyncedActionsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
