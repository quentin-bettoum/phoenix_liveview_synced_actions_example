defmodule SyncedActionsWeb.PageHTML do
  use SyncedActionsWeb, :html

  embed_templates "page_html/*"
end
