defmodule SyncedActionsWeb.FruitLive.Index do
  use SyncedActionsWeb, :live_view

  alias SyncedActions.Fruits
  alias SyncedActions.Fruits.Fruit
  alias SyncedActions.Accounts.User
  alias SyncedActionsWeb.{Endpoint, Presence, LiveHelpers}

  @topic "fruit_live"

  def topic, do: @topic

  @impl true
  def mount(_params, _session, socket) do
    Endpoint.subscribe(@topic)

    {:ok,
     socket
     |> stream(:fruits, list_fruits())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    cond do
      user = Presence.is_entity_used(@topic, id) ->
        error_message = "This fruit is already being edited by the user #{user.email}"

        socket
        |> put_flash(:error, error_message)
        |> push_patch(to: ~p"/fruits")

      true ->
        fruit = Fruits.get_fruit!(id)
        # Track when opening the entity
        Presence.track_live_entity(self(), @topic, socket.assigns.current_user, fruit)

        socket
        |> assign(:page_title, "Edit Fruit")
        |> assign(:fruit, fruit)
    end
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Fruit")
    |> assign(:fruit, %Fruit{})
  end

  defp apply_action(socket, :index, _params) do
    # Untrack when back on the list
    Presence.untrack(self(), @topic, socket.assigns.current_user.id)

    socket
    |> assign(:page_title, "Listing Fruits")
    |> assign(:fruit, nil)
  end

  @impl true
  def handle_info(%{event: "presence_diff"} = _diff, socket) do
    {:noreply, socket}
  end

  def handle_info(%{event: "list_update", payload: {operation, entity}}, socket) do
    entity = update_fruit_actions(entity)
    {:noreply, LiveHelpers.handle_list_update(socket, operation, entity)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    fruit = Fruits.get_fruit!(id)
    {:ok, _} = Fruits.delete_fruit(fruit)

    LiveHelpers.broadcast(@topic, :delete, fruit)
    {:noreply, stream_delete(socket, :fruits, fruit)}
  end

  defp list_fruits(), do: Fruits.list_fruits() |> update_list_actions()

  defp update_list_actions(fruits), do: Enum.map(fruits, &update_fruit_actions/1)

  defp update_fruit_actions(%Fruit{} = fruit) do
    if user = Presence.is_entity_used(@topic, fruit.id) do
      LiveHelpers.disable_entity_actions(fruit, "This fruit is being edited by #{user.email}")
    else
      LiveHelpers.enable_entity_actions(fruit)
    end
  end
end
