defmodule SyncedActions.Repo do
  use Ecto.Repo,
    otp_app: :synced_actions,
    adapter: Ecto.Adapters.Postgres
end
