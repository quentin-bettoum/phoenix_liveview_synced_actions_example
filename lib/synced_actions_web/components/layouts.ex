defmodule SyncedActionsWeb.Layouts do
  use SyncedActionsWeb, :html

  embed_templates "layouts/*"
end
