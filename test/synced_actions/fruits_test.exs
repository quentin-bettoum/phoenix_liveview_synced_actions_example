defmodule SyncedActions.FruitsTest do
  use SyncedActions.DataCase

  alias SyncedActions.Fruits

  describe "fruits" do
    alias SyncedActions.Fruits.Fruit

    import SyncedActions.FruitsFixtures

    @invalid_attrs %{color: nil, name: nil, quantity: nil}

    test "list_fruits/0 returns all fruits" do
      fruit = fruit_fixture()
      assert Fruits.list_fruits() == [fruit]
    end

    test "get_fruit!/1 returns the fruit with given id" do
      fruit = fruit_fixture()
      assert Fruits.get_fruit!(fruit.id) == fruit
    end

    test "create_fruit/1 with valid data creates a fruit" do
      valid_attrs = %{color: "some color", name: "some name", quantity: 42}

      assert {:ok, %Fruit{} = fruit} = Fruits.create_fruit(valid_attrs)
      assert fruit.color == "some color"
      assert fruit.name == "some name"
      assert fruit.quantity == 42
    end

    test "create_fruit/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Fruits.create_fruit(@invalid_attrs)
    end

    test "update_fruit/2 with valid data updates the fruit" do
      fruit = fruit_fixture()
      update_attrs = %{color: "some updated color", name: "some updated name", quantity: 43}

      assert {:ok, %Fruit{} = fruit} = Fruits.update_fruit(fruit, update_attrs)
      assert fruit.color == "some updated color"
      assert fruit.name == "some updated name"
      assert fruit.quantity == 43
    end

    test "update_fruit/2 with invalid data returns error changeset" do
      fruit = fruit_fixture()
      assert {:error, %Ecto.Changeset{}} = Fruits.update_fruit(fruit, @invalid_attrs)
      assert fruit == Fruits.get_fruit!(fruit.id)
    end

    test "delete_fruit/1 deletes the fruit" do
      fruit = fruit_fixture()
      assert {:ok, %Fruit{}} = Fruits.delete_fruit(fruit)
      assert_raise Ecto.NoResultsError, fn -> Fruits.get_fruit!(fruit.id) end
    end

    test "change_fruit/1 returns a fruit changeset" do
      fruit = fruit_fixture()
      assert %Ecto.Changeset{} = Fruits.change_fruit(fruit)
    end
  end
end
