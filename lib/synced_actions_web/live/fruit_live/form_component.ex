defmodule SyncedActionsWeb.FruitLive.FormComponent do
  use SyncedActionsWeb, :live_component

  alias SyncedActions.Fruits
  alias SyncedActionsWeb.FruitLive.Index
  alias SyncedActionsWeb.{Presence, LiveHelpers}

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage fruit records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="fruit-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" phx-debounce="500" />
        <.input field={@form[:color]} type="text" label="Color" phx-debounce="500" />
        <.input field={@form[:quantity]} type="number" label="Quantity" phx-debounce="500" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Fruit</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{fruit: fruit} = assigns, socket) do
    changeset = Fruits.change_fruit(fruit)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"fruit" => fruit_params}, socket) do
    changeset =
      socket.assigns.fruit
      |> Fruits.change_fruit(fruit_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"fruit" => fruit_params}, socket) do
    save_fruit(socket, socket.assigns.action, fruit_params)
  end

  defp save_fruit(socket, :edit, fruit_params) do
    case Fruits.update_fruit(socket.assigns.fruit, fruit_params) do
      {:ok, fruit} ->
        Presence.update_live_entity(self(), topic(), socket.assigns.current_user, fruit)

        {:noreply,
         socket
         |> put_flash(:info, "Fruit updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_fruit(socket, :new, fruit_params) do
    case Fruits.create_fruit(fruit_params) do
      {:ok, fruit} ->
        LiveHelpers.broadcast(topic(), :insert, fruit)

        {:noreply,
         socket
         |> put_flash(:info, "Fruit created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp topic, do: Index.topic()
end
