defmodule SyncedActions.Repo.Migrations.CreateFruits do
  use Ecto.Migration

  def change do
    create table(:fruits) do
      add :name, :string
      add :color, :string
      add :quantity, :integer

      timestamps()
    end
  end
end
